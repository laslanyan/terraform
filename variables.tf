variable "region" {
  description = "The region where resources will be created"
  type        = string
  default     = "us-central1"
}

variable "project_id" {
  description = "The ID of the project"
  type        = string
  default     = "gd-gcp-internship-devops"
}

variable "zone" {
  description = "The zone where resources will be created"
  type        = string
  default     = "us-central1-a"
}

variable "prefix" {
  description = "Prefix for resource names"
  type        = string
  default     = "levon-aslanyan"
}

variable "timeout_sec" {
  description = "Timeout in seconds"
  type        = number
  default     = "10"
}

variable "session_affinity" {
  description = "Session affinity setting"
  type        = string
  default     = "NONE"
}

variable "port_name" {
  description = "Name of the port"
  type        = string
  default     = "http"
}

variable "protocol" {
  description = "Protocol to use"
  type        = string
  default     = "HTTP"
}

variable "port_range" {
  description = "Port range"
  type        = number
  default     = "8081"
}

variable "check_interval_sec" {
  description = "Interval in seconds for health checks"
  type        = number
  default     = "10"
}

variable "base_instance_name" {
  description = "Base name for instances"
  type        = string
  default     = "instance-template"
}

variable "target_size" {
  description = "Target size of the instance group"
  type        = number
  default     = "3"
}

variable "machine_type" {
  description = "Machine type"
  type        = string
  default     = "e2-medium"
}

variable "image" {
  description = "Image for instance template"
  type        = string
  default     = "debian-cloud/debian-12"
}

variable "db_password" {
  type      = string
  sensitive = true
}