resource "google_compute_network" "vpc_network" {
  name                    = "vpc-levon-aslanyan"
  auto_create_subnetworks = true
  mtu                     = 1460
}

resource "google_compute_firewall" "rules" {
  name    = "firewall-levon-aslanyan"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["80", "8081", "22"]
  }

  source_ranges = ["0.0.0.0/0"]
}