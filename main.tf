terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 5.6"
    }
  }
}

provider "google" {
  project = var.project_id
  region  = var.region
}

module "vpc_network" {
  source = "./network"
}

resource "google_compute_instance_template" "levon-template" {
  name           = "${var.prefix}-instance-template"
  machine_type   = var.machine_type
  can_ip_forward = false

  scheduling {
    preemptible       = false
    automatic_restart = true
  }

  tags = ["owner-laslanyan", "env-prod"]

  disk {
    boot         = true
    source_image = var.image
  }

  network_interface {
    network = module.vpc_network.vpc_network
    access_config {}
  }
}

resource "google_compute_instance_group_manager" "levon-instance-group" {
  name               = "${var.prefix}-instance-group"
  base_instance_name = var.base_instance_name
  zone               = var.zone

  version {
    instance_template = google_compute_instance_template.levon-template.self_link
  }

  target_size = var.target_size

  named_port {
    name = var.port_name
    port = var.port_range
  }
}


resource "google_compute_global_address" "static-levon-ip" {
  name = "${var.prefix}-ip"
}

resource "google_compute_global_forwarding_rule" "levon-forwarding-rule" {
  name       = "${var.prefix}-forwarding-rule-port-8081"
  ip_address = google_compute_global_address.static-levon-ip.address
  port_range = var.port_range
  target     = google_compute_target_http_proxy.levon-http-proxy.self_link
}

resource "google_compute_target_http_proxy" "levon-http-proxy" {
  name    = "${var.prefix}-http-proxy"
  url_map = google_compute_url_map.levon-url-map.self_link
}

resource "google_compute_url_map" "levon-url-map" {
  name            = "${var.prefix}-url-map"
  default_service = google_compute_backend_service.levon-backend-service.self_link
}

resource "google_compute_backend_service" "levon-backend-service" {
  name             = "${var.prefix}-backend-service"
  protocol         = var.protocol
  port_name        = var.port_name
  timeout_sec      = var.timeout_sec
  session_affinity = var.session_affinity

  backend {
    group = google_compute_instance_group_manager.levon-instance-group.instance_group
  }

  health_checks = [google_compute_http_health_check.levon-healthcheck.self_link]
}

resource "google_compute_http_health_check" "levon-healthcheck" {
  name         = "${var.prefix}-healthcheck"
  request_path = "/"

  timeout_sec        = var.timeout_sec
  check_interval_sec = var.check_interval_sec
  port               = var.port_range

  lifecycle {
    create_before_destroy = true
  }
}