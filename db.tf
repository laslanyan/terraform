resource "google_sql_database_instance" "levon_db_instance" {
  name             = "${var.prefix}-mysql"
  region           = "us-central1"
  database_version = "MYSQL_8_0"

  settings {
    tier = "db-n1-standard-1"
    ip_configuration {
      ipv4_enabled = true
      require_ssl  = false

      authorized_networks {
        name  = "network"
        value = "0.0.0.0/0"
      }
    }
  }
  deletion_protection = false
}


resource "google_sql_database" "levon-database" {
  name     = "${var.prefix}-database"
  instance = google_sql_database_instance.levon_db_instance.name
}

resource "google_sql_user" "users" {
  name     = "root"
  instance = google_sql_database_instance.levon_db_instance.name
  password = var.db_password
  host     = "%"
}